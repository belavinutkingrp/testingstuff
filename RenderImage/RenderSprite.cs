﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DynamicObjects.RenderImage
{
    public class RenderSprite
    {
        private Border border;
        private Image image;

        public Border Border
        {
            get { return border; }
            set { border = value; }
        }
        public Image Image
        {
            get 
            { 
                return image; 
            }
            set
            {
                image = value;
            }
        }


        public RenderSprite()
        {
            this.border = new Border()
            {
                Width = 100,
                Height = 100,
                BorderThickness = new Thickness(5),
                BorderBrush = Brushes.Transparent
            };
            
            this.image = new Image();
            border.Child = image;
        }

        

        
        

    }
}
