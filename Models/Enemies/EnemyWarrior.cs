﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using DynamicObjects.Models.Utilities;
using DynamicObjects.RenderImage;

namespace DynamicObjects.Models.Enemies
{
    public enum Faction { IsEnemy}
    internal class EnemyWarrior
    {
        static string[] enemyWarriorImagePaths = { "pack://application:,,,/Images/SkellyRed3.png",
                                                   "pack://application:,,,/Images/SkellyBlue4.png", 
                                                   "pack://application:,,,/Images/SkellyGreen5.png",
                                                    };
        
        string name;
        string image; 
        public string Name { get; set; }
        public Faction Faction { get; set; }
        public SpawnPoint SpawnPoint { get; set; }

        private RenderSprite renderSprite;
        public RenderSprite RenderSprite
        {
            get { return renderSprite; }
            set { renderSprite = value; }
        }

        public EnemyWarrior(int imageIndex)
        {
            name = "EnemyWarrior";
            Faction = Faction.IsEnemy;
            image = enemyWarriorImagePaths[imageIndex];
            renderSprite = new RenderSprite();
            renderSprite.Image.Source = new BitmapImage(new Uri(image));

        }
    }


    

}
