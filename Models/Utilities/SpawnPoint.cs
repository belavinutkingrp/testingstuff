﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DynamicObjects.Models.Utilities
{
    internal struct SpawnPoint
    {
        Point point;
        internal Point Point
        {
            get { return point; }
            set { point = value; }
        }
        public SpawnPoint(double x, double y)
        {
            point.X = x;
            point.Y = y;

        }
    }
}
