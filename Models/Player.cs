﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using DynamicObjects.RenderImage;

namespace DynamicObjects.Models
{
    public enum Faction { IsPlayer }
    public class Player
    {
       


        string name;
        string image = "pack://application:,,,/Images/Skelly1.png";

        public string Name { get; set; }
        public Faction Faction { get; set; }

        private RenderSprite renderSprite; 
        public RenderSprite RenderSprite
        {
            get { return renderSprite; }
            set { renderSprite = value; }
        }
        public Player()
        {
            name = "Player";

            Faction = Faction.IsPlayer;
            renderSprite = new RenderSprite();
            //renderSprite.Image.Source = new BitmapImage(new Uri("Images/Skelly1.png", UriKind.Relative));

            renderSprite.Image.Source = new BitmapImage(new Uri(image));
           
            


            

        }
    }
}
