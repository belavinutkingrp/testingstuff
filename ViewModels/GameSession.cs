﻿using DynamicObjects.Models;
using DynamicObjects.Models.Enemies;
using DynamicObjects.Models.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace DynamicObjects.ViewModels
{
    public class GameSession : INotifyPropertyChanged
    {
        //public List<Player> playerList = new List<Player>;

        public string mouseCoords;

        public string MouseCoords
        {
            get { return mouseCoords; }
            set
            {
                
                    mouseCoords = value;
                    OnPropertyChanged(nameof(MouseCoords));
                
            }
        }

        SpawnPoint[] enemySpawnPoints = { new SpawnPoint(141, 53), new SpawnPoint(266, 53), new SpawnPoint(395, 53) };


        internal List<EnemyWarrior> enemies;

        public Player player;
        public Player Player
        {
            get { return player; }
            set
            {
                player = value;
                OnPropertyChanged(nameof(Player));
            }
        }

        public bool IsPlayerSpawned
        {
            get
            {
                if (Player is not null)
                {
                    return true;
                }
                return false;
            }
        }
        public GameSession()
        {
            enemies = new List<EnemyWarrior>();
        }

        public void CreatePlayer(object sender, MouseEventArgs e)
        {
            Canvas myCanvas = (Canvas)sender;
            
            Player = new Player();
            

            Point point = Mouse.GetPosition(myCanvas);

            Canvas.SetLeft(player.RenderSprite.Border, point.X - 50);
            Canvas.SetTop(player.RenderSprite.Border, point.Y - 50);
            MouseCoords = new string($"Mouse coords X: {point.X} Y: {point.Y}");
            myCanvas.Children.Add(player.RenderSprite.Border);
            
        }

       

        public void ShowPlayerBorder(object sender, MouseEventArgs e)
        {
           if (Player.Faction == Models.Faction.IsPlayer)
            {
                Player.RenderSprite.Border.BorderBrush = Brushes.YellowGreen;
            }
        }
        private void HidePlayerBorder(object sender, MouseEventArgs e)
        {
            if (Player.Faction == Models.Faction.IsPlayer)
            {
                Player.RenderSprite.Border.BorderBrush = Brushes.Transparent;
            }
            //if (this.GetType() == typeof(EnemyWarrior))
            //{
            //    this
            //}
        }

        public void SpawnPlayer()
        {
            this.player = new Player();
            this.Player.RenderSprite.Image.MouseEnter += ShowPlayerBorder;
            this.Player.RenderSprite.Image.MouseLeave += HidePlayerBorder;
        }

        public void SpawnEnemies()
        {
            if (enemies.Count == 0)
            {
                for (int i = 0; i < enemySpawnPoints.Length; i++)
                {
                    EnemyWarrior warrior = new EnemyWarrior(i);
                    warrior.SpawnPoint = enemySpawnPoints[i];
                    enemies.Add(warrior);
                }
            }
        }
        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged(string propertyChanged)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyChanged));
        }

        
    }
}
