﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DynamicObjects.ViewModels;



namespace DynamicObjects
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        GameSession gameSession;

        //Brush customColor;
        //Random r = new Random();


        public MainWindow()
        {
            InitializeComponent();
            gameSession = new GameSession();
            DataContext = gameSession;
            
           
            
        }


        //private void TestImage(object sender, MouseButtonEventArgs e)
        //{
        //    Image image = new Image();
        //    image.Width = 100;
        //    image.Height = 100;
        //    BitmapImage bmp = new BitmapImage();
        //    image.Source = new BitmapImage(new Uri("Images/Skelly1.png", UriKind.Relative));
        //    Canvas.SetLeft(image, Mouse.GetPosition(testGrid).X - 25);
        //    Canvas.SetTop(image, Mouse.GetPosition(testGrid).Y - 25);
        //    testGrid.Children.Add(image);
        //}

        //private void AddOrRemoveItems(object sender, MouseButtonEventArgs e)
        //{
        //    if (e.OriginalSource is Rectangle activeRectange)
        //    {
        //        myCanvas.Children.Remove(activeRectange);
        //    }
        //    else
        //    {
        //        customColor = new SolidColorBrush(Color.FromRgb((byte)r.Next(0, 256), (byte)r.Next(0, 256), (byte)r.Next(0, 256)));
        //        Rectangle newRectangle = new Rectangle()
        //        {
        //            Width = 50,
        //            Height = 50,
        //            Fill = customColor,
        //            StrokeThickness = 3,
        //            Stroke = Brushes.Black
        //        };

        //        Canvas.SetLeft(newRectangle, Mouse.GetPosition(myCanvas).X - 25);
        //        Canvas.SetTop(newRectangle, Mouse.GetPosition(myCanvas).Y - 25);
        //        myCanvas.Children.Add(newRectangle);
        //    }
        //}

        private void AddItemsOnGrid(object sender, MouseButtonEventArgs e)
        {
            gameSession.CreatePlayer(sender, e);

        }

        private void canvasOnLoad(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show($"Height: {testGrid.ActualHeight} Width: {testGrid.ActualWidth}");
        }

        private void OnClick_SpawnPlayer(object sender, RoutedEventArgs e)
        {
            gameSession.SpawnPlayer();
            Canvas.SetLeft(gameSession.Player.RenderSprite.Border, 253);
            Canvas.SetTop(gameSession.Player.RenderSprite.Border, 266);
            testGrid.Children.Add(gameSession.Player.RenderSprite.Border);
        }

        private void OnClick_SpawnEnemies(object sender, RoutedEventArgs e)
        {
            
            gameSession.SpawnEnemies();
            for (int i = 0; i < gameSession.enemies.Count; i++)
            {
                Canvas.SetLeft(gameSession.enemies[i].RenderSprite.Border, gameSession.enemies[i].SpawnPoint.Point.X - 10);
                Canvas.SetTop(gameSession.enemies[i].RenderSprite.Border, gameSession.enemies[i].SpawnPoint.Point.Y);
                testGrid.Children.Add(gameSession.enemies[i].RenderSprite.Border);
            }
        }
    }
}
